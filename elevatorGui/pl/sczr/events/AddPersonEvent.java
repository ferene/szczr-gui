package pl.sczr.events;

import pl.sczr.mockups.PersonMockup;


public class AddPersonEvent extends Event {

	/**
	 * W trybie automatycznym ten event wysylany jest przez Modul symulacji i
	 * odbierany przez GUI W trybie manualnym ten event wysylany jest przez GUI
	 * do Modulu symulacji, gdzie jest obslugiwany, a nastepnie odsylany
	 * spowrotem do GUI (jak w trybie automatycznym)
	 * 
	 * Gui wyswietla pojawienie sie mieszkanca doiero po dostaniu odpowiedzi o
	 * Modulu symulacji
	 */
 
	private static final long serialVersionUID = -2955798394651403750L;

	private final PersonMockup person;

	public AddPersonEvent(PersonMockup person) {
		this.person = person;
	}

	public AddPersonEvent(int floor, int destinationFloor) {
		this(new PersonMockup(floor, destinationFloor));
	}

	public PersonMockup getPerson() {
		return person;
	}

}
