package pl.sczr.events;

public class StartStopEvent extends Event {

	/**
	 * start = true - wlacz symulacje, start = false - zatrzymaj symulacje
	 */
	private static final long serialVersionUID = 1458724995250463778L;

	private final boolean start;

	public StartStopEvent(boolean start) {
		this.start = start;
	}

	public boolean isStart() {
		return start;
	}
}
