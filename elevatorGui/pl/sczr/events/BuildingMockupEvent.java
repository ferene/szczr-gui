package pl.sczr.events;

import java.util.List;
import java.util.Map;

import pl.sczr.mockups.PersonMockup;

public class BuildingMockupEvent extends Event {

	/**
	 * Event wysylany przez Modul symulacji do GUI, informuje o stanie
	 * wszystkich przyciskow i wszystkich mieszkancow, pozycji windy i jej celu
	 * jazdy.
	 * 
	 * Jednoznacznie oznacza ze winda rusza, zawarta jest informacja o tym kto
	 * znajduje sie w liscie
	 * 
	 */
	private static final long serialVersionUID = -598400794714413034L;

	// dane dotycz�ce zapalonych przyciskow
	private final List<Integer> elevatorButtons;
	private final List<Integer> upButtons;
	private final List<Integer> downButtons;

	private final Map<Integer, List<PersonMockup>> peopleUp;
	private final Map<Integer, List<PersonMockup>> peopleDown;

	private final List<PersonMockup> peopleInElevator;

	// dane dotyczace statusu windy
	private final int currentFloor;
	private final int destinationFloor;
	private final int rideTime;

	public BuildingMockupEvent(List<Integer> elevatorButtons,
			List<Integer> upButtons, List<Integer> downButtons,
			Map<Integer, List<PersonMockup>> peopleUp,
			Map<Integer, List<PersonMockup>> peopleDown,
			List<PersonMockup> peopleInElevator, int currentFloor,
			int destinationFloor, int rideTime) {

		this.elevatorButtons = elevatorButtons;
		this.upButtons = upButtons;
		this.downButtons = downButtons;
		this.peopleUp = peopleUp;
		this.peopleDown = peopleDown;
		this.currentFloor = currentFloor;
		this.destinationFloor = destinationFloor;
		this.rideTime = rideTime;
		this.peopleInElevator = peopleInElevator;

	}

	public List<PersonMockup> getPeopleInElevator() {
		return peopleInElevator;
	}

	public List<Integer> getElevatorButtons() {
		return elevatorButtons;
	}

	public List<Integer> getUpButtons() {
		return upButtons;
	}

	public List<Integer> getDownButtons() {
		return downButtons;
	}

	public Map<Integer, List<PersonMockup>> getPeopleUp() {
		return peopleUp;
	}

	public Map<Integer, List<PersonMockup>> getPeopleDown() {
		return peopleDown;
	}

	public int getCurrentFloor() {
		return currentFloor;
	}

	public int getDestinationFloor() {
		return destinationFloor;
	}

	public int getRideTime() {
		return rideTime;
	}

}
