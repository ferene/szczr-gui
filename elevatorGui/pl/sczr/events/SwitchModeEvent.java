package pl.sczr.events;

public class SwitchModeEvent extends Event {

	/**
	 * auto = true - tryb auto, auto = false - tryb manualny
	 */
	private static final long serialVersionUID = -5055624287580772263L;

	private final boolean auto;

	public SwitchModeEvent(boolean auto) {
		this.auto = auto;
	}

	public boolean isAuto() {
		return auto;
	}

}
