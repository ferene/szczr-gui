package pl.sczr.events;

public class SetGenerationTimeEvent extends Event {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1261709075305755479L;

	private final int minTime;
	private final int maxTime;

	public SetGenerationTimeEvent(int minTime, int maxTime) {
		this.minTime = minTime;
		this.maxTime = maxTime;
	}

	public int getMinTime() {
		return minTime;
	}

	public int getMaxTime() {
		return maxTime;
	}

}
