package pl.sczr.mockups;

import java.io.Serializable;

public class PersonMockup implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7210944098499944113L;

	private final int floor;
	private final int destinationFloor;

	public PersonMockup(int floor, int destinationFloor) {
		this.floor = floor;
		this.destinationFloor = destinationFloor;
	}

	public int getFloor() {
		return floor;
	}

	public int getDestinationFloor() {
		return destinationFloor;
	}

	boolean isGoingUp() {
		return destinationFloor > floor;
	}
}
