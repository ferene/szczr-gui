package pl.szczr.gui.model;

import java.util.LinkedList;

/**
 * klasa pietra
 * 
 * @author ferene
 *
 */
public class Floor 
{
	/* czy wcisniety przycisk w gore */
	private boolean upPressed;
	/* czy wciwniety przycisk na dol */
	private boolean downPressed;
	/* pasazerowie czekajacy na winde */
	private final LinkedList<Passenger> passengers;
	
	public Floor(final boolean isUpPressed, final boolean isDownPressed)
	{
		this.downPressed = isDownPressed;
		this.upPressed = isUpPressed;
		passengers = new LinkedList<Passenger>();
	}
	
	/**
	 * ustawienie stanu przycisku do gory
	 * 
	 * @param isPressed
	 */
	public void setUp(final boolean isPressed)
	{
		upPressed = isPressed;
	}
	
	/** 
	 * ustawienie stanu przycisku w dol
	 * 
	 * @param isPressed
	 */
	public void setDownPressed(final boolean isPressed)
	{
		downPressed = isPressed;
	}
	
	/**
	 * dodanie nowego pasazera czekajacego na winde
	 * 
	 * @param number - pietro na ktore pasazer chce dojechac
	 */
	public void addPassenger(final int number)
	{
		if(number >= 0 && number < 10)
		{
			passengers.add(new Passenger(number));
		}
	}
	
	/**
	 * info o tym czy przycisk do gory jest wciscniety
	 * 
	 * @return czy przycisk do gory jest wcisniety
	 */
	public boolean isUpPressed()
	{
		return upPressed;
	}
	
	/**
	 * info o tym czy przycisk do dolu jest wciscniety
	 * 
	 * @return czy przycisk do dolu jest wcisniety
	 */
	public boolean isDownPressed()
	{
		return downPressed;
	}
	
	/**
	 * zwraca liste pasazerow czekajacych na winde
	 * 
	 * @return lista pasazerow czekajacych na winde
	 */
	public LinkedList<Passenger> getPassengers()
	{
		return passengers;
	}
	
	/**
	 * meotda tworzaca kopie danego piertra
	 * 
	 * @param another - pietro do skopiowania
	 * @return - kopia piertra
	 */
	public static Floor newInstance(final Floor another)
	{
		Floor floor = new Floor(another.upPressed, another.downPressed);
		
		for(Passenger passenger: another.getPassengers())
		{
			floor.addPassenger(passenger.getFloor());
		}
		return floor;
	}
}
