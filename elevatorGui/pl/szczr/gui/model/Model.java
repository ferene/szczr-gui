package pl.szczr.gui.model;

import java.util.ArrayList;
import java.util.LinkedList;

import pl.sczr.events.BuildingMockupEvent;
import pl.sczr.mockups.PersonMockup;

/**
 * model
 * 
 * @author ferene
 *
 */
public class Model 
{
	/* pietra */
	private final ArrayList<Floor> floors;
	/* pasazerowie w windzie */
	private final LinkedList<Passenger> elevatorPassengers;
	/* przyciski w windzie */
	private final ArrayList<ElevatorButton> buttons;
	/* maksymalna ilosc osob jakie moga wejsc do windy */
	private final static int ELEVATOR_CAPACITY = 8;
	/* ilosc pieter budynku */
	private final static int FLOORS_NUMBER = 10;
	/* winda */
	private final Elevator elevator;
	
	/**
	 * konstruktor
	 */
	public Model()
	{
		buttons = new ArrayList<ElevatorButton>();
		floors = new ArrayList<Floor>();
		for(int i = 0; i<FLOORS_NUMBER; i++)
		{
			floors.add(new Floor(false, false));
			buttons.add(new ElevatorButton());
		}
		elevatorPassengers = new LinkedList<Passenger>();
		elevator = new Elevator();
	}
	
	/**
	 * metoda ustawiajaca stan przycisku do gory
	 * 
	 * @param indexOfButton - numer przycisku
	 * @param isPressed - czy jest wcisniety
	 */
	public void setUp(final int indexOfButton, final boolean isPressed)
	{ 
		if(indexOfButton < FLOORS_NUMBER && indexOfButton >0)
		{
			floors.get(indexOfButton).setUp(isPressed);	
		}
	}
	
	/**
	 * metoda ustawiajaca stan przycisku do dolu
	 * 
	 * @param indexOfButton - numer przycisku
	 * @param isPressed - czy jest wcisniety
	 */
	public void setDown(final int indexOfButton, final boolean isPressed)
	{
		if(indexOfButton < FLOORS_NUMBER && indexOfButton >0)
		{
			floors.get(indexOfButton).setDownPressed(isPressed);	
		}
	}

	/**
	 * metada zwaracajaca liste pasazerow windy
	 * 
	 * @return
	 */
	public LinkedList<Passenger> getElevatorPassengers() 
	{
		return elevatorPassengers;
	}
	
	/**
	 * meotda zwracajaca liste pietr
	 * 
	 * @return lista pietr
	 */
	public ArrayList<Floor> getFloors()
	{
		return floors;
	}
	
	/**
	 * metoda dodajaca pasazera do windy
	 * 
	 * @param floor - pietro na ktore pasazer chce dojechac
	 */
	public void addElevatorPassenger(final int floor)
	{
		if(elevatorPassengers.size() < ELEVATOR_CAPACITY)
		{
			elevatorPassengers.add(new Passenger(floor));
		}
	}
	
	/**
	 * metoda zwracajaca liste przyciskow windy
	 * 
	 * @return lista przyciskow windy
	 */
	public ArrayList<ElevatorButton> getElevatorButtons()
	{
		return buttons;
	}
	
	/**
	 * metoda ustawiajaca stan przycisku w windzie
	 * 
	 * @param index - numer przycisku odpowidajacy pietrze
	 * @param pressed - czy przycisk ma byc wcisniety
	 */
	public void setElevatorButtonPressed(final int index, final boolean pressed)
	{
		if(index < buttons.size() && index > 0)
		{
			buttons.get(index).setPressed(pressed);
		}
	}
	
	/**
	 * metoda dodajaca pasazera na pietrze
	 * 
	 * @param floorNumber - numer pietra na ktorym dodajemy pasazera
	 * @param whereTo - numer pietra na ktore pasazer chce dojechac
	 */
	public void addPassengerAtFloor(final int floorNumber, final int whereTo)
	{
		if(floors.size() > floorNumber && floors.size() > whereTo)
		{
			floors.get(floorNumber).addPassenger(whereTo);
		}
	}
	
	/**
	 * metoda dzieki ktorej pasazer wsiada do windy
	 * 
	 * @param floorNumber - pietro z ktorego pasazer wsiada
	 */
	public void getFirstPassengerIntoElevator(final int floorNumber)
	{
		if(floors.size() > floorNumber)
		{
			if(floors.get(floorNumber).getPassengers().size() > 0)
			{
				if(elevatorPassengers.size() < ELEVATOR_CAPACITY)
				{
					elevatorPassengers.add(floors.get(floorNumber).getPassengers().pollFirst());
				}
			}
		}
	}
	
	public void getPassengerIntoElevator(final int floorNumber)
	{
		Passenger remove = null;
		//gora
		if(getElevator().getKierunekJazdy() == KierunekJazdy.gora)
		{
			for(Passenger passenger: floors.get(floorNumber).getPassengers())
			{
				if(passenger.getFloor() > floorNumber)
				{
					elevatorPassengers.add(passenger);
					remove = passenger;
					break;
				}
			}
		}
		//w dol
		else
		{
			for(Passenger passenger: floors.get(floorNumber).getPassengers())
			{
				if(passenger.getFloor() < floorNumber)
				{
					elevatorPassengers.add(passenger);
					remove = passenger;
					break;
				}
			}
		}
		if(remove != null)
		{
			if(floors.get(floorNumber).getPassengers().contains(remove))
			{
				floors.get(floorNumber).getPassengers().remove(remove);
			}
		}
	}
	
	/**
	 * metoda wysiadania pasazerow z windy
	 * 
	 * @param floorNumber - piertro na ktore winda dojechala
	 */
	public void removeElevatorPassengers(final int floorNumber)
	{
		LinkedList<Passenger> passengersToRemove = new LinkedList<Passenger>();
		for(Passenger passenger: elevatorPassengers)
		{
			if(floorNumber == passenger.getFloor())
			{
				passengersToRemove.add(passenger);
			}
		}
		for(Passenger passenger: passengersToRemove)
		{
			elevatorPassengers.remove(passenger);
		}
	}
	
	public int getFloorsNumber()
	{
		return FLOORS_NUMBER;
	}

	public Elevator getElevator() 
	{
		return elevator;
	}
	
	public void updateMockup(final BuildingMockupEvent mockup)
	{
		for(Floor floor: floors)
		{
			floor.getPassengers().clear();
			floor.setDownPressed(false);
			floor.setUp(false);
		}
		elevatorPassengers.clear();
		
		for(ElevatorButton button: buttons)
		{
			button.setPressed(false);
		}
		//System.out.print("Przyciski w windzie:\n");
		//przyciski w windzie
		System.out.println("przyciski w windzie: " + mockup.getElevatorButtons());
		for(Integer floor: mockup.getElevatorButtons())
		{
			buttons.get( floor ).setPressed( true);
		}
		
		//przyciski przy pietrach - GORA
		System.out.println("przyciski gora: " + mockup.getUpButtons());
		for(Integer floor: mockup.getUpButtons())
		{
			floors.get( floor ).setUp(true);
		}
		
		//przyciski przy pietrach - DOL
		System.out.println("przyciski dol: " + mockup.getDownButtons());
		for(Integer floor: mockup.getDownButtons())
		{
			floors.get( floor ).setDownPressed(true);
		}
		
		//ludzie w windzie
		for(PersonMockup person: mockup.getPeopleInElevator())
		{
			elevatorPassengers.add(new Passenger(person.getDestinationFloor()));
		}
		
		//ludzie na pietrach
		for(int i = 0; i < FLOORS_NUMBER; i++)
		{
			for(PersonMockup person: mockup.getPeopleUp().get(i))
			{
				floors.get(i).addPassenger(person.getDestinationFloor());
			}
			
			for(PersonMockup person: mockup.getPeopleDown().get(i))
			{
				floors.get(i).addPassenger(person.getDestinationFloor());
			}
		}
		
		//winda
		elevator.stopAtFloor(mockup.getCurrentFloor());
		//jedz na dol
		if(mockup.getCurrentFloor() > mockup.getDestinationFloor())
		{
			elevator.goDown();
		}
		//na gore
		else
		{
			elevator.goUp();
		}
		elevator.setCurrentSpeed( ((mockup.getDestinationFloor() - mockup.getCurrentFloor())*100.f)/((float)mockup.getRideTime()) );
		System.out.println("speed " + ((mockup.getDestinationFloor() - mockup.getCurrentFloor())*100.f)/((float)mockup.getRideTime()));
		
	}
}
