package pl.szczr.gui.model;

/**
 * klasa windy - odpowiada za jej polozenie w szybie windy
 * zakladamy ze miedzy pietrami jest 100 jednostek odleglosci
 * czyli pietra znajduja sie na wielokrotnosci 100 zaczynajac od 0
 * 
 * dodatnia predkosc oznacza ze widna jedzie do gory czyli distance rosnie
 * ujemna analogicznie
 * 
 * @author ferene
 */
public class Elevator 
{
	/* miejsce w ktorym sie znajduje w szybie */
	private float distance;
	/* predkosc windy - [jednostka odleglosci/milisekunde] */
	private final static float SPEED = 0.1f;
	/* aktualna predkosc poruszania - uwzglednia zwrot */
	private float currentSpeed;
	/* czas w ktorym winda zaczela sie poruszac */
	private long startTime;
	private KierunekJazdy kierunekJazdy;
	
	/**
	 * konstruktor
	 * na poczatku winda domyslnie jest na pietrze nr 0 i nie porusza sie
	 */
	public Elevator() 
	{
		distance = 0;
		currentSpeed = 0;
		startTime = System.currentTimeMillis();
	}
	
	/**
	 * metora zatrzymania windy na konkretnym pietrze
	 * 
	 * @param floorNumber - numer pietra
	 */
	public void stopAtFloor(final int floorNumber)
	{
		currentSpeed = 0;
		distance = floorNumber*100;
	}
	
	/**
	 * @return polozenie windy
	 */
	public float getDistance()
	{
		return distance;
	}
	
	/**
	 * @return aktualna predkosc windy
	 */
	public float getSpeed()
	{
		return currentSpeed;
	}
	
	/**
	 * rozpoczecie jazdy w gore
	 */
	public void goUp()
	{
		if(currentSpeed == 0)
		{
			currentSpeed = SPEED;
			startTime = System.currentTimeMillis();
		}
		else
		{
			stop();
		}
	}
	
	/**
	 * rozpoczecie jazdy w dol
	 */
	public void goDown()
	{
		if(currentSpeed == 0)
		{
			currentSpeed = -SPEED;
			startTime = System.currentTimeMillis();
		}
		else
		{
			stop();
		}	
	}
	
	/**
	 * zatrzymanie sie windy
	 */
	public void stop()
	{
		if(currentSpeed != 0)
		{
			System.out.println("prev " + distance);
			float dis = currentSpeed*((System.currentTimeMillis() - startTime)/1000.f);
			System.out.println(distance + " dis " + dis + " time " + (System.currentTimeMillis() - startTime)/1000.f);
			distance += dis;
			System.out.println(distance);
		}
		currentSpeed = 0;
	}
	
	public long getStartTime()
	{
		return startTime;
	}

	public KierunekJazdy getKierunekJazdy()
	{
		return kierunekJazdy;
	}

	public void setKierunekJazdy(final KierunekJazdy kierunekJazdy) 
	{
		this.kierunekJazdy = kierunekJazdy;
	}
	
	public void setCurrentSpeed(final float speed)
	{
		currentSpeed = speed;
	}
}
