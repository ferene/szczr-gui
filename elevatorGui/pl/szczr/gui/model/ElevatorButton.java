package pl.szczr.gui.model;

/**
 * klasa przycisku wewnatrz windy
 * 
 * @author ferene
 *
 */
public class ElevatorButton 
{
	/* wcisniecie przycisku*/
	private boolean pressed;
	
	/**
	 * konstruktor
	 */
	public ElevatorButton() 
	{
		pressed = false;
	}
	
	/**
	 * ustawienie stanu przycisku
	 * 
	 * @param isPressed
	 */
	public void setPressed(final boolean isPressed)
	{
		this.pressed = isPressed;
	}
	
	/**
	 * informacja o stanie przycisku
	 * 
	 * @return czt jest wcisniety
	 */
	public boolean isPressed()
	{
		return pressed;
	}
	
	/**
	 * metoda tworzaca kopie przycisku
	 * 
	 * @param another - przycisk do skopiowania
	 * @return - kopia przycisku
	 */
	public static ElevatorButton newInstance(final ElevatorButton another)
	{
		ElevatorButton button = new ElevatorButton();
		button.setPressed(another.isPressed());
		return button;
	}
}
