package pl.szczr.gui.model;

/**
 * klasa pasazera
 * 
 * @author ferene
 *
 */
public class Passenger 
{
	/* numer pietra na ktore pasazer chce dojechac*/
	private final int floor;
	
	/**
	 * konstruktor
	 * 
	 * @param floor - numer pietra
	 */
	public Passenger(final int floor)
	{
		this.floor = floor;
	}
	
	/**
	 * zwraca informacje o numerze pietra na ktore pasazer chce dojechac
	 * 
	 * @return numer pietra
	 */
	public int getFloor()
	{
		return floor;
	}
	
	/**
	 * metoda tworzaca kopie pasazera
	 * 
	 * @param another - pasazer do skopiowania
	 * @return - kopia pasazera
	 */
	public static Passenger newInstance(final Passenger another)
	{
		return new Passenger(another.getFloor());
	}
}
