package pl.szczr.gui;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import pl.szczr.gui.controller.Controller;
import pl.szczr.gui.controller.event.ControllerEvent;
import pl.szczr.gui.model.Model;
import pl.szczr.gui.view.View;

public class ElevatorGui 
{
	public static void main(String[] args) 
	{
		final Model model = new Model();
		final BlockingQueue<ControllerEvent> queue = new LinkedBlockingQueue<ControllerEvent>();
		final View view = new View(queue);
		final Controller controller = new Controller(model, view, queue);
		
		controller.run();
	}
}
