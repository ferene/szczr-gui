package pl.szczr.gui.controller;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.BlockingQueue;

import pl.sczr.events.AddPersonEvent;
import pl.sczr.events.BuildingMockupEvent;
import pl.sczr.events.Event;
import pl.sczr.events.NextStepEvent;
import pl.sczr.events.SetGenerationTimeEvent;
import pl.sczr.events.StartStopEvent;
import pl.sczr.events.SwitchModeEvent;
import pl.szczr.gui.controller.communication.ReciveEventCallback;
import pl.szczr.gui.controller.communication.TCPClient;
import pl.szczr.gui.controller.dto.Dto;
import pl.szczr.gui.controller.dto.ElevatorDto;
import pl.szczr.gui.controller.event.AddEvent;
import pl.szczr.gui.controller.event.AddUrlEvent;
import pl.szczr.gui.controller.event.ConnectEvent;
import pl.szczr.gui.controller.event.ControllerEvent;
import pl.szczr.gui.controller.event.FailEvent;
import pl.szczr.gui.controller.event.SetTimeEvent;
import pl.szczr.gui.controller.event.SimulationFailEvent;
import pl.szczr.gui.controller.event.SimulationModeEvent;
import pl.szczr.gui.controller.event.StartEvent;
import pl.szczr.gui.controller.event.StepEvent;
import pl.szczr.gui.controller.event.StopElevatorEvent;
import pl.szczr.gui.controller.event.StopEvent;
import pl.szczr.gui.controller.event.TestEvent;
import pl.szczr.gui.controller.event.TestEvent2;
import pl.szczr.gui.controller.event.TimerEvent;
import pl.szczr.gui.controller.event.transmittedEvent.TransmittedGetEvent;
import pl.szczr.gui.controller.timer.Reminder;
import pl.szczr.gui.controller.timer.TimeCounter;
import pl.szczr.gui.model.Model;
import pl.szczr.gui.view.View;
import pl.szczr.gui.view.dao.Dao;
import pl.szczr.gui.view.dao.ElevatorDao;


public class Controller
{
	private final static int PORT = 3002;
	private final static long REFRESH_RATE = 50;
	private final Model model;
	private final BlockingQueue<ControllerEvent> queue;
	private final View view;
	private final Map<Class<?extends ControllerEvent>, ControllerReactionStrategy> strategyMap;
	private final Map<Class<?extends Event>, TransmissionReactionStrategy> transmissionMap;
	
	private final Timer timer;
	private final TimeCounter counter;
	
	private String url = "localhost";
	private Reminder reminder;
	
	private TCPClient client;
	
	public Controller(final Model model, final View view, final BlockingQueue<ControllerEvent> queue)
	{
		this.model = model;
		this.view = view;
		this.queue = queue;
		Map <Class<? extends ControllerEvent>, ControllerReactionStrategy> map = 
				new HashMap<Class<? extends ControllerEvent>, ControllerReactionStrategy>();
		map.put(TimerEvent.class, new TimerStrategy());
		map.put(AddEvent.class, new addStrategy());
		map.put(SimulationModeEvent.class, new SimulationModeStrategy());
		map.put(SetTimeEvent.class, new SetTimeStrategy());
		map.put(StopEvent.class, new StopStrategy());
		map.put(StartEvent.class, new StartStrategy());
		map.put(ConnectEvent.class, new ConnectStrategy());
		map.put(AddUrlEvent.class, new addUrlStrategy());
		map.put(TransmittedGetEvent.class, new TransmittedGetEventStrategy());
		map.put(StopElevatorEvent.class, new StopElevatorStrategy());
		map.put(StepEvent.class, new NextStepStrategy());
		map.put(FailEvent.class, new FailStrategy());
		map.put(TestEvent.class, new TestStrategy());
		map.put(TestEvent2.class, new Test2Strategy());
		
		strategyMap = Collections.unmodifiableMap(map);
		
		Map <Class<?extends Event>, TransmissionReactionStrategy> transMap 
			= new HashMap<Class<? extends Event>, Controller.TransmissionReactionStrategy>();
		transMap.put(BuildingMockupEvent.class, new MockupStrategy());
		transMap.put(AddPersonEvent.class, new AddPersonStrategy());
		transmissionMap = Collections.unmodifiableMap(transMap);
		timer = new Timer();
		counter = new TimeCounter(queue);
		
		timerSchedule(REFRESH_RATE);
	}
	
	private void timerSchedule(final long time)
	{
		timer.schedule(counter, 1, time);
	}
	
	private void transferDto()
	{
		Dto dto = new Dto(model.getElevatorPassengers(), model.getFloors(), model.getElevatorButtons());
		view.setDao(new Dao(dto));
	}
	
	private void transferElevatorDto()
	{
		ElevatorDto dto = new ElevatorDto(model.getElevator().getSpeed(), model.getElevator().getDistance(),
				model.getElevator().getStartTime());
		view.setElevatorDao(new ElevatorDao(dto));
	}
	
	private void connect()
	{
		if(client == null)
		{
			try 
			{
				client = new TCPClient(url, PORT);
				client.AsyncListen(new ReciveEventCallback() 
				{	
					@Override
					public void EventRecived(final Event event) 
					{
						queue.add(new TransmittedGetEvent(event));
					}
				});
			} 
			catch (IOException e) 
			{
				System.out.println("nie udalo sie ustanowic polaczenia");
				client = null;
			}
		}
	}
	
	private abstract class ControllerReactionStrategy
	{
		public abstract void react(final ControllerEvent event);
	}
	
	private class TimerStrategy extends ControllerReactionStrategy
	{
		@Override
		public void react(final ControllerEvent event) 
		{
			view.repaint();
		}
	}
	
	private class addStrategy extends ControllerReactionStrategy
	{
		private AddEvent addEvent;
		
		@Override
		public void react(final ControllerEvent event) 
		{
			addEvent = (AddEvent) event;
			String text = addEvent.getText();
			try
			{
				Integer number = Integer.valueOf(text);
				if(number >=0 && number < model.getFloorsNumber() && number != addEvent.getFloorNumber())
				{
					System.out.println("Pietro " + addEvent.getFloorNumber() + " na " + number);
					if(client != null)
					{
						client.AsyncSend(new AddPersonEvent(addEvent.getFloorNumber(), number));
					}
				}
			}
			catch(NumberFormatException e)
			{}
		}
	}
		
	private class SimulationModeStrategy extends ControllerReactionStrategy
	{
		private SimulationModeEvent modeEvent;
		@Override
		public void react(final ControllerEvent event) 
		{
			modeEvent = (SimulationModeEvent) event;
			switch (modeEvent.getMode())
			{
			case Automatic:
				System.out.println("auto");
				if(client != null)
				{
					System.out.println("auto");
					client.AsyncSend(new SwitchModeEvent(true));
				}
				break;
			case Manual:
				System.out.println("manual");
				if(client != null)
				{
					client.AsyncSend(new SwitchModeEvent(false));			
				}
				break;
			}
		}
	}
	
	private class SetTimeStrategy extends ControllerReactionStrategy
	{
		private SetTimeEvent timeEvent;

		@Override
		public void react(ControllerEvent event) 
		{
			timeEvent = (SetTimeEvent) event;
			
			String minTime = timeEvent.getMinTime();
			String maxTime = timeEvent.getMaxTime();
			
			try{
				Integer minTimeNumber = Integer.valueOf(minTime);
				Integer maxTimeNumber = Integer.valueOf(maxTime); 
				if(minTimeNumber >=0 && maxTimeNumber >=0)
				{
					if(client != null)
					{
						client.AsyncSend(new SetGenerationTimeEvent(minTimeNumber, maxTimeNumber));
					}
					System.out.println("min " + minTimeNumber + " max: " + maxTimeNumber);
				}
			}
			catch(NumberFormatException e)
			{}
		}
	}
	
	private class StopStrategy extends ControllerReactionStrategy
	{
		@Override
		public void react(ControllerEvent event) 
		{
			System.out.println("stop");	
			if(client != null)
			{
				client.AsyncSend(new StartStopEvent(false));
			}
		}
	}
	
	private class StartStrategy extends ControllerReactionStrategy
	{
		@Override
		public void react(ControllerEvent event) 
		{
			System.out.println("start");	
			if(client != null)
			{
				client.AsyncSend(new StartStopEvent(true));				
			}
		}	
	}
	
	private class ConnectStrategy extends ControllerReactionStrategy
	{
		@Override
		public void react(ControllerEvent event) 
		{
			connect();	
		}	
	}
	
	private class addUrlStrategy extends ControllerReactionStrategy
	{
		private AddUrlEvent urlEvent;
		@Override
		public void react(ControllerEvent event) 
		{
			urlEvent = (AddUrlEvent) event;
			url = urlEvent.getUrl();
			System.out.println("ustawiam url na: " + url);
		}
	}
	
	private class TransmittedGetEventStrategy extends ControllerReactionStrategy
	{
		private TransmittedGetEvent transmittedEvent;
		
		@Override
		public void react(final ControllerEvent event) 
		{
			transmittedEvent = (TransmittedGetEvent) event;
			transmissionMap.get(transmittedEvent.getEvent().getClass()).react(transmittedEvent.getEvent());
		}
	}
	
	public class StopElevatorStrategy extends ControllerReactionStrategy
	{
		private StopElevatorEvent stopEvent;
		
		@Override
		public void react(final ControllerEvent event) 
		{
			stopEvent = (StopElevatorEvent) event;
			model.getElevator().stopAtFloor(stopEvent.getFloorNumber());
			transferElevatorDto();
			reminder.cancel();
		}
	}
	
	private class NextStepStrategy extends ControllerReactionStrategy
	{
		@Override
		public void react(ControllerEvent event) 
		{
			System.out.println("next step");	
			if(client != null)
			{
				client.AsyncSend(new NextStepEvent());				
			}		
		}
	}
	
	private class FailStrategy extends ControllerReactionStrategy
	{
		@Override
		public void react(ControllerEvent event) 
		{
			if(client != null)
			{
				client.AsyncSend(new SimulationFailEvent());
			}
			System.out.println("Fail event!");
		}
		
	}
	
	private class TestStrategy extends ControllerReactionStrategy
	{
		@Override
		public void react(final ControllerEvent event) 
		{
			model.addPassengerAtFloor(3, 8);
			model.addPassengerAtFloor(2, 0);
			model.addElevatorPassenger(3);
			model.addElevatorPassenger(3);
			model.addElevatorPassenger(7);
			model.addElevatorPassenger(2);
			model.addElevatorPassenger(1);
			model.addElevatorPassenger(3);
			
			model.setElevatorButtonPressed(3, true);

			transferElevatorDto();
			
			transferDto();
		}
	}
	
	private class Test2Strategy extends ControllerReactionStrategy
	{
		@Override
		public void react(ControllerEvent event) 
		{		
			model.getElevator().goUp();
			transferElevatorDto();
		}
	}
	
	//------------------STRATEGIE TRANSMISJI----------------------------
	private abstract class TransmissionReactionStrategy
	{
		public abstract void react(final Event event);
	}
	
	private class MockupStrategy extends TransmissionReactionStrategy
	{
		private BuildingMockupEvent mockupEvent;
		@Override
		public void react(final Event event) 
		{
			mockupEvent = (BuildingMockupEvent) event;
			model.updateMockup(mockupEvent);
			reminder = new Reminder(mockupEvent.getRideTime(), queue, mockupEvent.getDestinationFloor());
			transferDto();
			transferElevatorDto();
		}
	}
	
	private class AddPersonStrategy extends TransmissionReactionStrategy
	{
		private AddPersonEvent personEvent;
		@Override
		public void react(final Event event) 
		{
			personEvent = (AddPersonEvent) event;
			model.addPassengerAtFloor(personEvent.getPerson().getFloor(), 
					personEvent.getPerson().getDestinationFloor());
			//jedzie na gore
			if(personEvent.getPerson().getDestinationFloor() > personEvent.getPerson().getFloor())
			{
				model.getFloors().get(personEvent.getPerson().getFloor()).setUp(true);
			}
			else
			{
				model.getFloors().get(personEvent.getPerson().getFloor()).setDownPressed(true);
			}
			transferDto();
		}
	}
	
	public void run() 
	{
		while(true)
		{
			try 
			{	
				ControllerEvent event = queue.take();
				strategyMap.get(event.getClass()).react(event);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
				throw new RuntimeException();
			}
		}
	}
}
