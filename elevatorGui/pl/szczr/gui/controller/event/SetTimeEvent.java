package pl.szczr.gui.controller.event;

public class SetTimeEvent extends ControllerEvent
{
	private final String minTime;
	private final String maxTime;
	
	public SetTimeEvent(final String minTime, final String maxTime) 
	{
		this.minTime = minTime;
		this.maxTime = maxTime;
	}

	public String getMinTime() 
	{
		return minTime;
	}

	public String getMaxTime() 
	{
		return maxTime;
	}
}
