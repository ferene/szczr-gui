package pl.szczr.gui.controller.event;

public class StopElevatorEvent extends ControllerEvent
{
	private final int floorNumber;
	
	public StopElevatorEvent(final int floorNumber) 
	{
		this.floorNumber = floorNumber;
	}

	public int getFloorNumber() 
	{
		return floorNumber;
	}

}
