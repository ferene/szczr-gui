package pl.szczr.gui.controller.event;

public class AddUrlEvent extends ControllerEvent
{
	private final String url;
	
	public AddUrlEvent(final String url) 
	{
		this.url = url;
	}

	public String getUrl() 
	{
		return url;
	}
}
