package pl.szczr.gui.controller.event.transmittedEvent;

import pl.sczr.events.Event;
import pl.szczr.gui.controller.event.ControllerEvent;

/**
 * event sieciowy
 * 
 * @author ferene
 */
public class TransmittedGetEvent extends ControllerEvent
{
	private final Event event;
	
	public TransmittedGetEvent(final Event event) 
	{
		this.event = event;
	}

	public Event getEvent() 
	{
		return event;
	}

}
