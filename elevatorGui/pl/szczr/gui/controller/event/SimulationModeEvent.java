package pl.szczr.gui.controller.event;

import pl.szczr.gui.view.SimulationMode;

public class SimulationModeEvent extends ControllerEvent
{
	private final SimulationMode mode;
	
	public SimulationModeEvent(final SimulationMode mode) 
	{
		this.mode = mode;
	}

	public SimulationMode getMode() 
	{
		return mode;
	}

}
