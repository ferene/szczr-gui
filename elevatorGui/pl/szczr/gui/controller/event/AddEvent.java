package pl.szczr.gui.controller.event;

/**
 * event checi dodania nowego pasazera
 * @author ferene
 */
public class AddEvent extends ControllerEvent
{
	/* tekst wpisany w oknie*/
	private final String text;
	/* numer pietra na ktorym chcemy dodac pasazera */
	private final int floorNumber;
	
	public AddEvent(final String text, final int floorNumber)
	{
		this.text = text;
		this.floorNumber = floorNumber;
	}

	public String getText() 
	{
		return text;
	}

	public int getFloorNumber() 
	{
		return floorNumber;
	}

}
