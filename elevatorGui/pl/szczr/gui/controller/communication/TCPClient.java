package pl.szczr.gui.controller.communication;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import pl.sczr.events.AddPersonEvent;
import pl.sczr.events.Event;

/**
 * The Class TCPClient.
 */
public class TCPClient {

	/** The output. */
	private ObjectOutputStream output;

	/** The input. */
	private ObjectInputStream input;

	/** The client. */
	private Socket client;

	/**
	 * Instantiates a new tCP client.
	 * 
	 * @param addres
	 *            the addres
	 * @param port
	 *            the port
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public TCPClient(final String addres, final int port) throws IOException {
		client = new Socket(addres, port);
		output = new ObjectOutputStream(client.getOutputStream());
		input = new ObjectInputStream(client.getInputStream());
	}

	/**
	 * Send.
	 * 
	 * @param event
	 *            the event
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void Send(final Event event) throws IOException {
		synchronized (this) {

			output.writeObject(event);
			output.flush();
		}
	}

	/**
	 * Async send.
	 * 
	 * @param event
	 *            the event
	 */
	public void AsyncSend(final Event event) {
		new Thread() {

			@Override
			public void run() {
				try {
					Send(event);
				} catch (IOException e) {
					System.err.println("ERROR: Async sending failed!");
					e.printStackTrace();
				}
			}
		}.start();
	}

	/**
	 * Read event.
	 * 
	 * @return the event
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Event readEvent() throws ClassNotFoundException, IOException 
	{
			return (Event) input.readObject();
	}

	/**
	 * Async recive line.
	 * 
	 * @param callback
	 *            the callback
	 */
	public void AsyncRecive(final ReciveEventCallback callback) {
		new Thread() {

			@Override
			public void run() {
				try {
					Event event = readEvent();
					callback.EventRecived(event);
				} catch (ClassNotFoundException | IOException e) {
					System.err.println("ERROR: Async reciving failed!");
					e.printStackTrace();
				}
			}
		}.start();
	}

	public void AsyncListen(final ReciveEventCallback callback) {
		new Thread() {

			@Override
			public void run() {
				while (true) {
					try {
						Event event = readEvent();
						callback.EventRecived(event);
					} catch (ClassNotFoundException | IOException e) {
						System.err.println("ERROR: Async reciving failed!");
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	/**
	 * Close.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void close() throws IOException {
		input.close();
		output.close();
	}

}