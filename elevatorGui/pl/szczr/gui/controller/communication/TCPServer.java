package pl.szczr.gui.controller.communication;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import pl.sczr.events.Event;

/**
 * The Class TCPServer.
 */
public class TCPServer {

	/** The output. */
	private ObjectOutputStream output;

	/** The input. */
	private ObjectInputStream input;

	/** The client. */
	private Socket client;

	/** The server. */
	private ServerSocket server;

	/**
	 * Instantiates a new tCP server.
	 * 
	 * @param port
	 *            the port
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public TCPServer(int port) throws IOException {
		server = new ServerSocket(port);
		client = server.accept();
		output = new ObjectOutputStream(client.getOutputStream());
		input = new ObjectInputStream(client.getInputStream());
	}

	/**
	 * Send.
	 * 
	 * @param event
	 *            the event
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void Send(Event event) throws IOException {
		output.writeObject(event);
		output.flush();
	}

	/**
	 * Read event.
	 * 
	 * @return the event
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Event readEvent() throws ClassNotFoundException, IOException {
		return (Event) input.readObject();
	}

	/**
	 * Close.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void close() throws IOException {
		server.close();
		input.close();
		output.close();
	}

}
