package pl.szczr.gui.controller.communication;

import pl.sczr.events.Event;

/**
 * The Interface ReciveEventCallback.
 */
public interface ReciveEventCallback {

	/**
	 * Event recived.
	 *
	 * @param event the event
	 */
	abstract void EventRecived(Event event);
}
