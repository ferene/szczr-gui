package pl.szczr.gui.controller.timer;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;

import pl.szczr.gui.controller.event.ControllerEvent;
import pl.szczr.gui.controller.event.StopElevatorEvent;

public class Reminder 
{
	private final BlockingQueue<ControllerEvent> queue;
	private Timer timer;
	private final int floorNumber;
	
	public Reminder(final long miliSeconds, final BlockingQueue<ControllerEvent> queue, final int floorNumber)
	{
		this.floorNumber = floorNumber;
		this.queue = queue;
		timer = new Timer();
		if(miliSeconds < 0)
		{
			timer.cancel();
		}
		else
		{
			timer.schedule(new RemindTask(), miliSeconds);
		}
		
	}
	
	class RemindTask extends TimerTask
	{
		@Override
		public void run() 
		{
			queue.add(new StopElevatorEvent(floorNumber));
			timer.cancel();
		}
	}
	
	public void cancel()
	{
		timer.cancel();
	}
}