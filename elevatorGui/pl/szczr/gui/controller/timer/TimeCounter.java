package pl.szczr.gui.controller.timer;

import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;

import pl.szczr.gui.controller.event.ControllerEvent;
import pl.szczr.gui.controller.event.TimerEvent;

public class TimeCounter extends TimerTask
{
private final BlockingQueue<ControllerEvent> queue;
	
	public TimeCounter(final BlockingQueue<ControllerEvent> queue)
	{
		this.queue = queue;
	}
	
	@Override
	public void run() 
	{
		queue.add(new TimerEvent());	
	}
}
