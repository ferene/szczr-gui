package pl.szczr.gui.controller.dto;

import java.util.ArrayList;
import java.util.LinkedList;

import pl.szczr.gui.model.ElevatorButton;
import pl.szczr.gui.model.Floor;
import pl.szczr.gui.model.Passenger;

/**
 * Dto
 * 
 * @author ferene
 */
public class Dto 
{
	/* pasazerowie windy*/
	private final LinkedList<Passenger> passengers;
	/* pietra */
	private final ArrayList<Floor> floors;
	/* przyciski w windzie */
	private final ArrayList<ElevatorButton> buttons;
	
	public Dto(final LinkedList<Passenger> passengers, final ArrayList<Floor> modelFloors,
			final ArrayList<ElevatorButton> buttons)
	{
		this.passengers = new LinkedList<Passenger>();
		this.floors = new ArrayList<Floor>();
		this.buttons = new ArrayList<ElevatorButton>();
		
		
		for(Passenger passenger: passengers)
		{
			this.passengers.add(Passenger.newInstance(passenger));
		}
		
		for(Floor floor: modelFloors)
		{
			this.floors.add(Floor.newInstance(floor));
		}
		
		for(ElevatorButton button: buttons)
		{
			this.buttons.add(ElevatorButton.newInstance(button));
		}
	}

	public LinkedList<Passenger> getPassengers() 
	{
		return passengers;
	}

	public ArrayList<Floor> getFloors() 
	{
		return floors;
	}
	
	public ArrayList<ElevatorButton> getButtons()
	{
		return buttons;
	}

}
