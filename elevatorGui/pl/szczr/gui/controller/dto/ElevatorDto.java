package pl.szczr.gui.controller.dto;

public class ElevatorDto 
{
	private final float speed;
	private final float distance;
	private final long startTime;
	
	public ElevatorDto(final float speed, final float distance, final long startTime) 
	{
		this.speed = speed;
		this.distance = distance;
		this.startTime = startTime;
	}

	public float getSpeed() 
	{
		return speed;
	}

	public float getDistance() 
	{
		return distance;
	}

	public long getStartTime() 
	{
		return startTime;
	}
	

}
