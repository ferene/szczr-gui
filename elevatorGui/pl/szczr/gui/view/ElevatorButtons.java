package pl.szczr.gui.view;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class ElevatorButtons 
{
	private final ArrayList<JLabel> buttons;
	
	public ElevatorButtons(final int count) 
	{
		buttons = new ArrayList<JLabel>();
		for(int i = 0; i<count; i++)
		{
			JLabel label = new JLabel();
			try
			{
				label.setIcon( new ImageIcon(getClass().getClassLoader().getResource("buttons/" + i +"NotPressed.png")) );	
			}
			catch(NullPointerException e)
			{
				System.out.println("nie udalo sie wczytac obrazka \n" + e);
			}
			buttons.add(label);
		}
	}
	
	public void setPressed(final boolean isPressed, final int index)
	{
		if(index >=0)
		{
			String pressed;
			if(isPressed)
			{
				pressed = "";
			}
			else
			{
				pressed = "Not";
			}
			buttons.get(index).setIcon(new ImageIcon("img/buttons/" + index + pressed +"Pressed.png"));
			try
			{
				buttons.get(index).setIcon(new ImageIcon(getClass().getClassLoader().getResource("buttons/" + index + pressed +"Pressed.png")));
			}
			catch(NullPointerException e)
			{
				System.out.println("nie udalo sie wczytac obrazka \n" +e);
			}
		}
	}
	
	public ArrayList<JLabel> getButtons()
	{
		return buttons;
	}

}
