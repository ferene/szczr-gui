package pl.szczr.gui.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class ElevatorPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	/* [jedn/milisekunda] */
	private static float SPEED = 2500;
//	private BufferedImage elevatorGo;
//	private BufferedImage elevatorStop;
//	private BufferedImage elevator;
	
	private float currentSpeed;
	private float distance;
	private long startTime = System.currentTimeMillis();
	private final static float FLOOR_HEIGHT = 49;
	
	private ImageIcon elevatorGo;
	private ImageIcon elevatorStop;
	private ImageIcon elevator;
	
	public ElevatorPanel() 
	{
		currentSpeed = 0;
		setDistance(0);
		try
		{
			elevatorGo = new ImageIcon(getClass().getClassLoader().getResource("elevatorGo.png"));
			//new ImageIcon(getClass().getClassLoader().getResource("elvatorGo.png"));
			//elevatorGo = new ImageIcon(getClass().getClassLoader().getResource("elevatorGo.png"));
			elevatorStop = new ImageIcon(getClass().getClassLoader().getResource("elevatorStop.png"));
		}
		catch(NullPointerException e)
		{
			System.out.println("nie udalo sie wczytac obrazka " + e);
		}
		setPreferredSize(new Dimension(50, 10000));
		elevator = elevatorStop;
	}
	
	public void paintComponent(Graphics g)
	{
		Graphics2D g2d = (Graphics2D)g;
		elevator.paintIcon(null, g2d,10, (int) (distance - (Math.round((System.currentTimeMillis() - startTime)*currentSpeed*FLOOR_HEIGHT/100))));
	}
	
	public void stopAtFloor(final int floorNumber)
	{
		distance = (9 -floorNumber) * FLOOR_HEIGHT;
		currentSpeed = 0;
		elevator = elevatorStop;
	}
	
	public void stop()
	{
		elevator = elevatorStop;
		distance += distance - (Math.round((System.currentTimeMillis() - startTime)*currentSpeed/FLOOR_HEIGHT)); 
		currentSpeed = 0;
	}
	
	public void goDown()
	{
		startTime = System.currentTimeMillis();
		currentSpeed = - SPEED;
		elevator = elevatorGo;
	}
	
	public void goUp()
	{
		startTime = System.currentTimeMillis();
		currentSpeed = SPEED;
		elevator = elevatorGo;
	}
	
	public void setDistance(final float distance)
	{
		this.distance = (900 - distance ) *FLOOR_HEIGHT/100;
	}
	
	public void setCurrentSpeed(final float currentSpeed)
	{
		if(currentSpeed == 0)
		{
			elevator = elevatorStop;
		}
		else
		{
			elevator = elevatorGo;
		}
		this.currentSpeed = currentSpeed;
	}
	
	public void setStartTime(final long startTime)
	{
		this.startTime = startTime;
	}
}
