package pl.szczr.gui.view;

import java.net.URL;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Floor 
{
	private ImageIcon upNotPressed;
	private ImageIcon downNotPressed;
	private ImageIcon upPressed;
	private ImageIcon downPressed;
	private ImageIcon passenger;
	
	private JLabel upButton;
	private JLabel downButton;
	private final JLabel addButton;
	
	private ImageIcon addNotPressed;
	private ImageIcon addPressed;
	
	
	private LinkedList<JLabel> passengers;
	
	public Floor()
	{
		try
		{
			upNotPressed = new ImageIcon(getClass().getClassLoader().getResource("upNotPressed.png"));
			upPressed = new ImageIcon(getClass().getClassLoader().getResource("upPressed.png"));
			downNotPressed = new ImageIcon(getClass().getClassLoader().getResource("downNotPressed.png"));
			downPressed = new ImageIcon(getClass().getClassLoader().getResource("downPressed.png"));
			passenger = new ImageIcon(getClass().getClassLoader().getResource("actor.png"));
			
			addNotPressed = new ImageIcon(getClass().getClassLoader().getResource("add.png"));
			addPressed = new ImageIcon(getClass().getClassLoader().getResource("addPressed.png"));	
		}
		catch(NullPointerException e)
		{
			System.out.println("Nie udalo sie wczytac obrazka \n" + e);
		}
		
		
				
		addButton = new JLabel();
		addButton.setIcon(addNotPressed);
		
		upButton = new JLabel();
		upButton.setIcon(upNotPressed);
		
		downButton = new JLabel();
		downButton.setIcon(downNotPressed);
		
		passengers = new LinkedList<JLabel>();
		for(int i = 0; i< 8; i++)
		{
			JLabel label = new JLabel("(" + i +")");
			label.setIcon(passenger);
			passengers.add(label);
		}
		
	}
	
	public void setDown(final boolean pressed)
	{
		if(pressed)
		{
			downButton.setIcon(downPressed);
		}
		else
		{
			downButton.setIcon(downNotPressed);
		}
	}
	
	public void setUp(final boolean pressed)
	{
		if(pressed)
		{
			upButton.setIcon(upPressed);
		}
		else
		{
			upButton.setIcon(upNotPressed);
		}
	}
	
	public JLabel getUpButton()
	{
		return upButton;
	}
	
	public JLabel getDownButton()
	{
		return downButton;
	}
	
	public JLabel getAddButton()
	{
		return addButton;
	}
	
	public LinkedList<JLabel> getPassengers()
	{
		return passengers;
	}
	
	public void setPassengerFloor(final int indexOfPassenger, final int floorNumber)
	{
		passengers.get(indexOfPassenger).setText("(" + floorNumber + ")");
	}
	
	/**
	 * zmienia wyglad przycisku do dodawania w zaleznosci od tego czy jest wcisniety
	 * 
	 * @param pressed - czy jest wcisniety
	 */
	public void setAddPressed(final boolean pressed)
	{
		if(pressed)
		{
			addButton.setIcon(addPressed);
		}
		else
		{
			addButton.setIcon(addNotPressed);
		}
		
	}

}
