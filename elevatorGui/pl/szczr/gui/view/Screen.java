package pl.szczr.gui.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import pl.szczr.gui.controller.event.AddEvent;
import pl.szczr.gui.controller.event.AddUrlEvent;
import pl.szczr.gui.controller.event.ConnectEvent;
import pl.szczr.gui.controller.event.ControllerEvent;
import pl.szczr.gui.controller.event.FailEvent;
import pl.szczr.gui.controller.event.SetTimeEvent;
import pl.szczr.gui.controller.event.SimulationModeEvent;
import pl.szczr.gui.controller.event.StartEvent;
import pl.szczr.gui.controller.event.StepEvent;
import pl.szczr.gui.controller.event.StopEvent;
import pl.szczr.gui.controller.event.TestEvent;
import pl.szczr.gui.controller.event.TestEvent2;
import pl.szczr.gui.model.Passenger;
import pl.szczr.gui.view.dao.Dao;
import pl.szczr.gui.view.dao.ElevatorDao;

public class Screen extends JFrame
{
	private static final long serialVersionUID = -7640495408891874075L;

	private final BlockingQueue<ControllerEvent> queue;
	
	private Dao dao;
	private ElevatorDao elevatorDao;
	private static final int ELEVATOR_HEIGHT = 59*200;
	private static final int FLOORS = 10;
	private static final int QUEUE_MAX_LENGTH = 8;
	private final static String ELEVATOR = "[" + ELEVATOR_HEIGHT + "px]";
	private JPanel klatkaSchodowa;
	private JPanel passengers;
	private JPanel buttonPanel;
	private JPanel dolnyPanel;
	private JPanel dolnyLewyPanel;
	private JPanel dolnyPrawyPanel;
	
	private StartButton startButton;
	private ButtonGroup autoGroup;
	
	private JTextField newPassengerFloor;
	private JTextField url;
	private JButton addUrl;
	private ElevatorPanel elevatorPanel;
	
	private JRadioButton manualRadioButton;
	
	private JRadioButton autoRadioButton;
	
	private ArrayList<Floor> floors;
	private final ElevatorPassengers elevatorPassengers;
	
	private final ElevatorButtons buttons;
		
	private JTextField minTimeField;
	private JButton setTimeButton;
	private JTextField maxTimeField;
	
	private JButton testButton;
	private JButton testButton2;
	private JButton connectButton;
	private JButton nextStepButton;
	
	private JButton simulationFail;
	
	
	public Screen(final BlockingQueue<ControllerEvent> queue) 
	{
		this.queue = queue;

		//setSize(800, 1000);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(10, 10));
		klatkaSchodowa = new JPanel();
		
		getContentPane().add(klatkaSchodowa, BorderLayout.WEST);
		klatkaSchodowa.setLayout(new MigLayout("", "[][][][][][][][][][]", ELEVATOR + ELEVATOR + ELEVATOR + ELEVATOR + ELEVATOR
				+ ELEVATOR + ELEVATOR + ELEVATOR + ELEVATOR + ELEVATOR));
		
		addFloorLabes();
		passengers = new JPanel();
		getContentPane().add(passengers, BorderLayout.NORTH);
		passengers.setLayout(new MigLayout());
		
		newPassengerFloor = new JTextField();
		newPassengerFloor.setToolTipText("pietro na ktore nowy pasazer chce dojechac");
		newPassengerFloor.setColumns(2);
		
		elevatorPassengers = new ElevatorPassengers(QUEUE_MAX_LENGTH);

		addPassengers();
		
		elevatorPanel = new ElevatorPanel();
		getContentPane().add(elevatorPanel, BorderLayout.CENTER);
				
		buttons = new ElevatorButtons(FLOORS);

		buttonPanel = new JPanel();
		getContentPane().add(buttonPanel, BorderLayout.EAST);
		buttonPanel.setLayout(new MigLayout("", "[][][]", "[][][][]"));
		
		for(JLabel button: buttons.getButtons())
		{
			if(buttons.getButtons().indexOf(button) ==0)
			{
				buttonPanel.add(button, "cell 2 4");
			}
			else
			{
				buttonPanel.add(button, "cell " + (3 - (buttons.getButtons().indexOf(button)-1)%3) + " " 
						+ (3-(buttons.getButtons().indexOf(button)-1)/3));
			}
		}
	
		dolnyPanel = new JPanel();
		getContentPane().add(dolnyPanel, BorderLayout.SOUTH);
		dolnyPanel.setLayout(new BorderLayout());
		dolnyLewyPanel = new JPanel();
		dolnyPrawyPanel = new JPanel();
		dolnyPanel.add(dolnyLewyPanel, BorderLayout.WEST);
		dolnyPanel.add(dolnyPrawyPanel, BorderLayout.CENTER);
		dolnyLewyPanel.setLayout(new MigLayout("","[][][][]"));
		dolnyPrawyPanel.setLayout(new MigLayout("", "[][][][]"));
		
		startButton = new StartButton();
		startButton.addMouseListener(new MouseListener() 
		{	
			@Override
			public void mouseReleased(MouseEvent arg0) 
			{
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) 
			{
				if(startButton.isStart())
				{
					startButton.setStop();
					queue.add(new StartEvent());
				}
				else
				{
					startButton.setStart();
					queue.add(new StopEvent());
				}
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) 
			{	
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) 
			{	
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) 
			{	
			}
		});
		dolnyLewyPanel.add(startButton.getButton(), "cell 0 1");
		
		autoRadioButton = new JRadioButton("Tryb automatyczny");
		autoRadioButton.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(autoRadioButton.isSelected())
				{
					queue.add(new SimulationModeEvent(SimulationMode.Automatic));
				}
			}
		});
		autoRadioButton.setSelected(true);
		autoGroup = new ButtonGroup();
		autoGroup.add(autoRadioButton);
		dolnyLewyPanel.add(autoRadioButton, "cell 0 2");
		
		manualRadioButton = new JRadioButton("Tryb reczny");
		manualRadioButton.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if(manualRadioButton.isSelected())
				{
					queue.add(new SimulationModeEvent(SimulationMode.Manual));
				}
			}
		});
		autoGroup.add(manualRadioButton);
		dolnyLewyPanel.add(manualRadioButton, "cell 0 4");
		
		minTimeField = new JTextField();
		minTimeField.setText("1000");
		minTimeField.setToolTipText("minTime");
		minTimeField.setColumns(10);
		dolnyLewyPanel.add(minTimeField, "cell 0 6");
		
		setTimeButton = new JButton();
		setTimeButton.setText("setTime"); 
		setTimeButton.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				queue.add(new SetTimeEvent(minTimeField.getText(), maxTimeField.getText()));
			}
		});
		dolnyLewyPanel.add(setTimeButton, "cell 2 6");
		
		maxTimeField = new JTextField();
		maxTimeField.setText("10000");
		maxTimeField.setToolTipText("maxTime");
		maxTimeField.setColumns(10);
		dolnyLewyPanel.add(maxTimeField, "cell 0 8");
		
		testButton = new JButton("test");
		url = new JTextField();
		url.setText("localhost");
		url.setColumns(20);
		dolnyPrawyPanel.add(url, "cell 0 6");
		//dolnyPrawyPanel.add(testButton, "cell 1 0");
		addUrl = new JButton("Dodaj url");
		addUrl.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				queue.add(new AddUrlEvent(url.getText()));
			}
		});
		dolnyPrawyPanel.add(addUrl, "cell 1 6");
		
		simulationFail = new JButton("awaria symulacji");
		simulationFail.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				queue.add(new FailEvent());
			}
		});
		
		dolnyPrawyPanel.add(simulationFail, "cell 1 8");
		
		
		testButton.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				queue.add(new TestEvent());	
			}
		});
		nextStepButton = new JButton("next Step");
		nextStepButton.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				queue.add(new StepEvent());	
			}
		});
		dolnyPrawyPanel.add(nextStepButton, "cell 0 4");
		testButton2 = new JButton("test2");
		//dolnyPrawyPanel.add(testButton2, "cell 1 2");
		
		testButton2.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				queue.add(new TestEvent2());	
			}
		});
		
		connectButton = new JButton("Connect");
		dolnyPrawyPanel.add(connectButton, "cell 1 4");
		connectButton.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				queue.add(new ConnectEvent());
			}
		});
		pack();
		setResizable(false);

	}
	
	private void addFloorLabes()
	{
		floors = new ArrayList<Floor>();
		for(int i = 0; i< FLOORS; i++)
		{
			floors.add(new Floor());
		}
		
		for(final Floor floor: floors)
		{
			for(JLabel passenger: floor.getPassengers())
			{
				klatkaSchodowa.add(passenger, "cell " + (QUEUE_MAX_LENGTH - 1 -floor.getPassengers().indexOf(passenger) ) +
						" " + (FLOORS - 1 - floors.indexOf(floor)) );
				passenger.setVisible(false);
			}
			
			floor.getAddButton().addMouseListener(new MouseListener() 
			{	
				@Override
				public void mouseReleased(MouseEvent arg0) 
				{	
					floor.setAddPressed(false);
				}
				
				@Override
				public void mousePressed(MouseEvent arg0) 
				{
					floor.setAddPressed(true);
					queue.add(new AddEvent(newPassengerFloor.getText(), floors.indexOf(floor)));
				}
				
				@Override
				public void mouseExited(MouseEvent arg0) 
				{
				}
				
				@Override
				public void mouseEntered(MouseEvent arg0) 
				{	
				}
				
				@Override
				public void mouseClicked(MouseEvent arg0) 
				{
				}
			});
			klatkaSchodowa.add(floor.getAddButton(), "cell 8 " + (FLOORS - 1 - floors.indexOf(floor)));
			
			if(floor != floors.get(FLOORS-1))
			{
				klatkaSchodowa.add(floor.getUpButton(), "cell 9 " + (FLOORS - 1 - floors.indexOf(floor)));
			}
			
			if(floors.indexOf(floor) != 0)
			{
				klatkaSchodowa.add(floor.getDownButton(), "cell 10 " + (FLOORS - 1 - floors.indexOf(floor)));
			}
			
			JLabel napis = new JLabel("Pietro " + floors.indexOf(floor));
			klatkaSchodowa.add(napis, "cell 11 " + (FLOORS - 1 - floors.indexOf(floor)));
		}
	}
	
	/**
	 * metoda uaktualniajaca widok po zaktualizowaniu dao
	 */
	public void refreshDao()
	{
		//ustawienie wszystkich na niewidocznych
		for(JLabel pass: elevatorPassengers.getPassengers())
		{
			pass.setVisible(false);
		}
		
		//ustawienie wybranych z dao na widocznych i przypisanie im odpowiednich numerow pieter
		for(Passenger passenger: dao.getPassengers())
		{
			elevatorPassengers.setFloor(dao.getPassengers().indexOf(passenger), passenger.getFloor());
			elevatorPassengers.getPassengers().get(dao.getPassengers().indexOf(passenger)).setVisible(true);
		}
		
		//ustawienie wszystkich na niewidoczne
		for(Floor floor: floors)
		{
			for(JLabel passenger: floor.getPassengers())
			{
				passenger.setVisible(false);
			}
		}
		
		//ustawienie wybranych z dao na widoczne i przypisanie numerow
		for(Floor floor: floors)
		{
			floor.setDown(dao.getFloors().get(floors.indexOf(floor)).isDownPressed());
			floor.setUp(dao.getFloors().get(floors.indexOf(floor)).isUpPressed());
			
			for(Passenger passenger: dao.getFloors().get(floors.indexOf(floor)).getPassengers())
			{
				if(dao.getFloors().get(floors.indexOf(floor)).getPassengers().indexOf(passenger) < QUEUE_MAX_LENGTH)
				{
					floor.setPassengerFloor(dao.getFloors().get(floors.indexOf(floor)).getPassengers().indexOf(passenger), 
							passenger.getFloor());
					floor.getPassengers().get(dao.getFloors().get(floors.indexOf(floor)).getPassengers().indexOf(passenger)).setVisible(true);	
				}
			}
		}
		
		// przyciski w windzie
		for(JLabel button: buttons.getButtons())
		{
			buttons.setPressed(dao.getButtons().get(buttons.getButtons().indexOf(button)).isPressed(), 
					buttons.getButtons().indexOf(button) );
		}
	}
	
	/**
	 * metoda uaktualniajaca widok szybu windy
	 */
	public void refreshElevatorDao()
	{
		elevatorPanel.stop();
		elevatorPanel.setDistance(elevatorDao.getDistance());
		elevatorPanel.setStartTime(elevatorDao.getStartTime());
		elevatorPanel.setCurrentSpeed(elevatorDao.getSpeed());
	}
	
	private void addPassengers()
	{
		passengers.removeAll();
		passengers.setLayout(new MigLayout());
		passengers.add(newPassengerFloor);
		passengers.add(new JLabel("Pasazerowie: "));
		
		for(JLabel passenger: elevatorPassengers.getPassengers())
		{
			passengers.add(passenger);
			passenger.setVisible(false);
		}
	}
	
	public void setDao(final Dao dao)
	{
		this.dao = dao;
	}
	
	public void setElevatorDao(final ElevatorDao dao)
	{
		this.elevatorDao = dao;
	}
}
