package pl.szczr.gui.view;

import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class ElevatorPassengers 
{
	private final LinkedList<JLabel> passengers;
	private final ImageIcon passengerIcon;
	
	public ElevatorPassengers(final int passengersLimit) 
	{
		passengers = new LinkedList<JLabel>();
		passengerIcon = new ImageIcon(getClass().getClassLoader().getResource("actor.png"));
		for(int i = 0; i<passengersLimit; i++)
		{
			JLabel label = new JLabel("(" + i +")");
			label.setIcon(passengerIcon);
			passengers.add(label);
		}
	}
	
	public LinkedList<JLabel> getPassengers()
	{
		return passengers;
	}
	
	public void setFloor(final int indexOfPassenger, final int floorNumber)
	{
		if(indexOfPassenger < passengers.size())
		{
			passengers.get(indexOfPassenger).setText("(" + floorNumber + ")");
		}
	}

}
