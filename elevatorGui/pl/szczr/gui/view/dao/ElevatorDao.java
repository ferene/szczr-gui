package pl.szczr.gui.view.dao;

import pl.szczr.gui.controller.dto.ElevatorDto;

public class ElevatorDao 
{
	private final float speed;
	private final float distance;
	private final long startTime;
	
	public ElevatorDao(final ElevatorDto dto) 
	{
		this.speed = dto.getSpeed();
		this.distance = dto.getDistance();
		this.startTime = dto.getStartTime();
	}

	public float getSpeed() 
	{
		return speed;
	}

	public float getDistance() 
	{
		return distance;
	}

	public long getStartTime() 
	{
		return startTime;
	}

}
