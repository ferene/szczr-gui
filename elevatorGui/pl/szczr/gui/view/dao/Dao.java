package pl.szczr.gui.view.dao;

import java.util.ArrayList;
import java.util.LinkedList;

import pl.szczr.gui.controller.dto.Dto;
import pl.szczr.gui.model.ElevatorButton;
import pl.szczr.gui.model.Floor;
import pl.szczr.gui.model.Passenger;

/**
 * Dao
 * 
 * @author ferene
 */
public class Dao 
{
	/* pasazerowie windy*/
	private final LinkedList<Passenger> passengers;
	/* pietra */
	private final ArrayList<Floor> floors;
	/* przyciski w windzie */
	private final ArrayList<ElevatorButton> buttons;
	
	/**
	 * konstruktor
	 */
	public Dao(final Dto dto)
	{
		this.passengers = dto.getPassengers();
		this.floors = dto.getFloors();
		this.buttons = dto.getButtons();
		
	}

	/**
	 * @return liste pasazerow windy
	 */
	public LinkedList<Passenger> getPassengers() 
	{
		return passengers;
	}
	
	/**
	 * @return piertra
	 */
	public ArrayList<Floor> getFloors()
	{
		return floors;
	}
	
	public ArrayList<ElevatorButton> getButtons()
	{
		return buttons;
	}
}
