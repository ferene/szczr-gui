package pl.szczr.gui.view;

import java.util.concurrent.BlockingQueue;

import javax.swing.SwingUtilities;

import pl.szczr.gui.controller.event.ControllerEvent;
import pl.szczr.gui.view.dao.Dao;
import pl.szczr.gui.view.dao.ElevatorDao;

public class View 
{
	private Screen screen;
	
	public View(final BlockingQueue<ControllerEvent> queue)
	{
		SwingUtilities.invokeLater(new Runnable() 
		{
			@Override
			public void run() 
			{
				screen = new Screen(queue);
			}
		});
	}
	
	public void repaint()
	{
		if(screen!=null)
		{
			screen.repaint();
		}
	}
	
	public void setDao(final Dao dao)
	{
		SwingUtilities.invokeLater(new Runnable() 
		{	
			@Override
			public void run() 
			{
				if(screen != null)
				{
					screen.setDao(dao);	
					screen.refreshDao();
				}	
			}
		});
	}
	
	public void setElevatorDao(final ElevatorDao dao)
	{
		SwingUtilities.invokeLater(new Runnable() 
		{	
			@Override
			public void run() 
			{
				if(screen != null)
				{
					screen.setElevatorDao(dao);
					screen.refreshElevatorDao();
				}
			}
		});
	}
}
