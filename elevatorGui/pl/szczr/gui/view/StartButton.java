package pl.szczr.gui.view;

import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class StartButton 
{
	private final JLabel button;
	private ImageIcon start;
	private ImageIcon stop;
	
	public StartButton() 
	{
		try
		{
			start = new ImageIcon(getClass().getClassLoader().getResource("play.png"));
			stop = new ImageIcon(getClass().getClassLoader().getResource("stop.png"));
				
		}
		catch(NullPointerException e)
		{
			System.out.println("Nie udalo sie wczytac obrazka\n" +e);
		}
		button = new JLabel();
		setStart();
	}
	
	public void setStart()
	{
		button.setIcon(start);
	}
	
	public void setStop()
	{
		button.setIcon(stop);
	}
	
	public JLabel getButton()
	{
		return button;
	}
	
	public void addMouseListener(final MouseListener listener)
	{
		button.addMouseListener(listener);
	}
	
	public boolean isStart()
	{
		return button.getIcon() == start;
	}

}
